<?php
include 'db-connect.php';

$result = $connect->prepare("SELECT * FROM appearance LIMIT 0, 100");
$result->execute();
while( $row = $result->fetch(PDO::FETCH_ASSOC)){
    $type_id = str_split($row['type']);
    $image_output='';
    $color_output='';
    if($type_id[2]==='0'){   
       $image_output= "<div class='formrow clearfix'>
                  <div class='input_image'>
                    <label>".$row['label']."</label>
                    <span class='info'>".$row['info']."</span>
                    <input type='file' id='".$row['id_value']."' name='".$row['name']."' class='image_input' data-default='".$row['value']."'>
                  </div>
                  <div class='small_preview'>
                    <img id='".$row['name']."_small' src='".$row['value']."' alt=''>
                    <img src='../images/trash_recyclebin_empty_closed.png' alt='recyclebin' class='remove_image''>
                  </div>
                </div> " ;
    }else{
        $color_output="<div class='colorinput'>
                  <label>".$row['label']."</label>
                  <input type='text' class='colorpicker' name='".$row['name']."' value='".$row['value']."' itemprop='".$type_id[2]."'>
                </div>";
    }
    switch ($type_id[0]){
        case 1:
            switch ($type_id[1]){
            case 1:
                if(!isset($tab1_1_images)){$tab1_1_images = $image_output;}else{$tab1_1_images .= $image_output;}
                if(!isset($tab1_1_colors)){$tab1_1_colors = $color_output;}else{$tab1_1_colors .= $color_output;}
                break;
            case 2:
                if(!isset($tab1_2_images)){$tab1_2_images = $image_output;}else{$tab1_2_images .= $image_output;}
                if(!isset($tab1_2_colors)){$tab1_2_colors = $color_output;}else{$tab1_2_colors .= $color_output;}
                break;
            case 3:
                if(!isset($tab1_3_images)){$tab1_3_images = $image_output;}else{$tab1_3_images .= $image_output;}
                if(!isset($tab1_3_colors)){$tab1_3_colors = $color_output;}else{$tab1_3_colors .= $color_output;}
                break;
            }
            break;
        case 2:
            switch($type_id[1]){
            case 1:
                if(!isset($tab2_1_images)){$tab2_1_images = $image_output;}else{$tab2_1_images .= $image_output;}
                if(!isset($tab2_1_colors)){$tab2_1_colors = $color_output;}else{$tab2_1_colors .= $color_output;}
                break; 
            case 2:
                if(!isset($tab2_2_images)){$tab2_2_images = $image_output;}else{$tab2_2_images .= $image_output;}
                if(!isset($tab2_2_colors)){$tab2_2_colors = $color_output;}else{$tab2_2_colors .= $color_output;}
                break;
            case 3:
                if(!isset($tab2_3_images)){$tab2_3_images = $image_output;}else{$tab2_3_images .= $image_output;}
                if(!isset($tab2_3_colors)){$tab2_3_colors = $color_output;}else{$tab2_3_colors .= $color_output;}
                break;
            case 4:
                if(!isset($tab2_4_images)){$tab2_4_images = $image_output;}else{$tab2_4_images .= $image_output;}
                if(!isset($tab2_4_colors)){$tab2_4_colors = $color_output;}else{$tab2_4_colors .= $color_output;}
                break; 
            }
            break;
        case 3:
            switch($type_id[1]){
            case 1:
                if(!isset($tab3_1_images)){$tab3_1_images = $image_output;}else{$tab3_1_images .= $image_output;}
                if(!isset($tab3_1_colors)){$tab3_1_colors = $color_output;}else{$tab3_1_colors .= $color_output;}
                break; 
            case 2:
                if(!isset($tab3_2_images)){$tab3_2_images = $image_output;}else{$tab3_2_images .= $image_output;}
                if(!isset($tab3_2_colors)){$tab3_2_colors = $color_output;}else{$tab3_2_colors .= $color_output;}
                break;
            case 3:
                if(!isset($tab3_3_images)){$tab3_3_images = $image_output;}else{$tab3_3_images .= $image_output;}
                if(!isset($tab3_3_colors)){$tab3_3_colors = $color_output;}else{$tab3_3_colors .= $color_output;}
                break;
            case 4:
                if(!isset($tab3_4_images)){$tab3_4_images = $image_output;}else{$tab3_4_images .= $image_output;}
                if(!isset($tab3_4_colors)){$tab3_4_colors = $color_output;}else{$tab3_4_colors .= $color_output;}
                break; 
            }
            break;
            case 4:
                if(!isset($tab4_1_images)){$tab4_1_images = $image_output;}else{$tab4_1_images .= $image_output;}
                if(!isset($tab4_1_colors)){$tab4_1_colors = $color_output;}else{$tab4_1_colors .= $color_output;}
                break;
            case 5:
                if(!isset($tab5_1_images)){$tab5_1_images = $image_output;}else{$tab5_1_images .= $image_output;}
                if(!isset($tab5_1_colors)){$tab5_1_colors = $color_output;}else{$tab5_1_colors .= $color_output;}
                break;
        } 
    }
?>
<html>
  <head>
    <title>TapThemer</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width initial-scale=1">
    <!-- stylesheets -->
    <link rel='stylesheet' type='text/css' href="../css/spectrum.css"/>
    <link rel="stylesheet" type="text/css" href="../css/tapthemer.css"/>
    <!-- scripts -->
    <!--[if IE]>
      <script type="text/javascript" src="vendor/classList.js"></script>
      <script type="text/javascript" src="vendor/modernizr.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../vendor/jquery-1.9.1.js"></script>
    <script type='text/javascript' src='../vendor/spectrum.js'></script>
    <script type="text/javascript" src="../js/tapthemer.js"></script>
  </head>
  <body>
    <div class="overall_container clearfix">
      <h1>Edit appearance</h1>
      <div id="tabContainer">
        <div class="tabs">
          <ul>
            <li id="tabHeader_1" class="tab" data-target="#prev_25">Launcher</li>
            <li id="tabHeader_2" class="tab" data-target="#prev_27">Content</li>
            <li id="tabHeader_3" class="tab" data-target="#preview_extra">Extra</li>
            <li id="tabHeader_4" class="tab" data-target="#preview_store">Store</li>
            <li id="tabHeader_5" class="tab" data-target="#preview_details">Details</li>
          </ul>
        </div>
        <form id="user_inputs" action="../js_form_ontvanger.html" method="GET" accept-charset="utf-8" enctype="multipart/form-data">
          <input type="submit" value="submit"/>
          <div class="tabscontent">
            <div class="tabpage" id="tabpage_1">
              
              <fieldset>
                <h2>
                  <span class='sectionheader'>Launcher background</span>
                </h2>
                <?php
                print ($tab1_1_images);
                print ($tab1_1_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class='sectionheader'>Navigation bar</span>
                </h2>
                <?php
                print ($tab1_2_images);
                print ($tab1_2_colors);
                ?>
              </fieldset>
              <fieldset class='clearfix'>
                <h2>
                  <span class='sectionheader'>Icons</span>
                </h2>
                <?php
                print ($tab1_3_images);
                print ($tab1_3_colors);
                ?>
              </fieldset>
            </div>
            <div class="tabpage" id="tabpage_2">
              
              <fieldset class="clearfix">
                <h2>
                  <span class="sectionheader">General</span>
                </h2>
                <?php
                print ($tab2_1_images);
                print ($tab2_1_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class='sectionheader'>Separators-Titles-Subheaders</span>
                </h2>
                <?php
                print ($tab2_2_images);
                print ($tab2_2_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class='sectionheader'>Cells</span>
                </h2>
                <?php
                print ($tab2_3_images);
                print ($tab2_3_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class='sectionheader'> Lists</span>
                </h2>
                <?php
                print ($tab2_4_images);
                print ($tab2_4_colors);
                ?>
              </fieldset>
            </div>
            <div class="tabpage" id="tabpage_3">
              
              <fieldset>
                <h2>
                  <span class="sectionheader">tabs on top of a view</span>
                </h2>
                <span class="info">Example: Conference bag, personal programme</span>
               <?php
                print ($tab3_1_images);
                print ($tab3_1_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class='sectionheader'>Navigation</span>
                </h2>
                <?php
                print ($tab3_2_images);
                print ($tab3_2_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class="sectionheader">Buttons</span>
                </h2>
                <?php
                print ($tab3_3_images);
                print ($tab3_3_colors);
                ?>
              </fieldset>
              <fieldset>
                <h2>
                  <span class="sectionheader">Action bar</span>
                </h2>
                <?php
                print ($tab3_4_images);
                print ($tab3_4_colors);
                ?>
              </fieldset>
            </div>
            <div class="tabpage" id="tabpage_4">
              
              <fieldset>
                  <div class="enable_preview">
                    <a href="" class="enable_disable enabled" data-target="prev_4_image"></a>
                  </div>
                <?php
                print ($tab4_1_images);
                print ($tab4_1_colors);
                ?>
              </fieldset>
            </div>
            <div class="tabpage" id="tabpage_5">
              <fieldset>
                <h2>
                  <span class="sectionheader">Various</span>
                </h2>
               <?php
                print ($tab5_1_images);
                print ($tab5_1_colors);
                ?>
              </fieldset>
            </div>
          </div>
        </form>
      </div>
      <div id="preview_main_wrapper">
        <div id="preview_header">
          <img src="" alt="" id='prev_6_image'/>
          <img src="" alt="" id='prev_9_image'/>
          <canvas width="40" height="38" id="prev_15"></canvas>
        </div>
        <div id="prev_25">
          <img src='' alt='' id='prev_11_image'>
          <div id="preview_launcher_content">
            <canvas id="prev_23" width="285" height="100"></canvas>
            <div id='prev_13'>
              <span class="icon_text">info</span>
              <span class="icon_text">location</span>
              <span class="icon_text">loyalty</span> 
            </div>
            <img src='../images/logo.png' alt='' id='tapcrowd_logo'>
          </div>
        </div>
        <div id="prev_27">
          <div id="prev_3">
            <p id='prev_7'>Title</p>
          </div>
          <div id="prev_29">
            <select id ="prev_30">
              <option value="">Subheader</option>
            </select>
          </div>
          <div id='prev_1'>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
          </div>
          <ul class="list">
            <li id="prev_33" class="prev_60"><img src='../images/website-small.png' alt='website-small'>Action cell</li>
            <li id="prev_10" class="prev_60"><img src='../images/phone-small.png' alt='phone-small'>Selected cell</li>
          </ul>
          <div id="prev_2">
            <span id=prev_8>Separator</span>
          </div>
          <ul class="list">
            <li id='prev_47' class="prev_60"><img src="../images/email-small.png" alt='email-small' id='prev_12_image'><span id='prev_48'>Premium cell</span></li>
          </ul> 
        </div>
        <div id='preview_extra' class='bg_color'>
          <ul class='prev_tab clearfix'>
            <li id='prev_37'><span id='prev_38'>tab</span></li>
            <li id='prev_39'><span id='prev_40'>highlighted</span></li>
          </ul>
          <img id= prev_16_image src='' alt="../google-maps marker">
          <img src ="../images/google_maps_example.png" alt="google maps">
          <img src="../images/floorplan.jpg" alt ="floorplan">
          <img id="prev_32_image" src="" alt="../floorplan-marker">
          <div id="prev_49" class="prev_57">
            <span id="prev_56">Button</span>
          </div>
          <div id="prev_62">
            <span id="prev_63">actionbar</span>
          </div>
        </div>
        <div id="preview_store" class="">
          <img src="" id="prev_4_image" alt="">
          <ul>
            <li><img src="../images/app_icon_error1.png" alt="app icon error"><span>No rounded corners</span></li>
            <li><img src="../images/app_icon_error2.png" alt="app icon error"> No glossy effect</li>
            <li><img src="" alt="app icon ok" id="prev_5_image"></li>
          </ul>  
        </div>
      </div> <!-- end preview wrapper -->
    </div> <!-- end content wrapper -->
  </body>
</html>
