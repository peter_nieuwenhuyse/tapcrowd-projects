/*
 * 
 * created by peter nieuwenhuyse on 09-01-2014
 * purpose: create live preview functions for the tapcrowd appearance page
 */

$(function(){
    /*
     * loop through all ellements and apply their value to the correspondent
     * preview box
     */ 
    $('.image_input').each(function(){
        $('#prev_'+this.name).attr('src',this.getAttribute('data-default'));
        $('#prev_'+this.name).css('display','block');
    });
    
    $('.colorpicker').each(function(){
        $('#prev_'+ this.name).css('display','block');
        var prop = this.getAttribute('itemprop');
        if(prop === '1'){
            $('#prev_' + this.name).css('background-color','#' + this.value);
            if(this.name==='39'){
                $('.prev_tab').css('border-color','#' + this.value);
            }
        }else{
            if(prop === '2'){
                $('#prev_' + this.name).css('color','#' + this.value);  
            }else{
                if(prop === '3'){
                    $('#prev_' + this.name).css('border-color','#' + this.value);  
                }
            }
        }
    });
    
    //change events
    $('.image_input').on('change',function(){
        read_preview_URL(this);
    }); 
    $('.colorpicker').spectrum({
        preferredFormat: "hex6",
        showInput: true,
        allowEmpty:true,
        showPalette:true,
        showAlpha:true,
        localStorageKey:"spectrum.themeroller",
        palette:[
            ['black', 'white', 'blanchedalmond'],
            ['rgb(255, 128, 0);', 'hsv 100 70 50', 'lightyellow']
        ],
        change:function(color){
            /*
             * itemprop 1 = background color
             * itemprop 2 = text color
             * itemprop 3 = border color
             * itemprop 4 = image color
             */
            var target = '#prev_' + this.name;
            var color_val= color.toHexString();
            var value_set= color_val.split('#')[1];
            this.setAttribute('value',value_set);
            var prop = this.getAttribute('itemprop');
            if(prop !== '1'){
                if(prop !== '2'){
                    if(prop !== '3'){
                        //itemprop=4
                        var canvas = document.getElementById('prev_' + this.name);
                        var context = canvas.getContext('2d');
                        var rgb= color.toRgb();
                        var image_data=context.getImageData(0,0,canvas.width,canvas.height);
                        for (var i = 0; i < image_data.data.length; i += 4) {
                            if ( image_data.data[i] + image_data.data[i+1] + image_data.data[i+2]+image_data.data[i+3] !== 0 ){
                                image_data.data[i] = rgb.r;
                                image_data.data[i+1] = rgb.g;
                                image_data.data[i+2] = rgb.b;
                            }
                        }
                        context.putImageData(image_data, 0, 0);    
                    }else{
                        //itemprop =3
                        $('.prev_'+ this.name).css('border-color',color.toHexString());
                    }
                }else{
                    //itemprop=2
                    $(target).css('color',color.toHexString());
                }
            }else{
                //itemprop=1
                $(target).css('background-color',color.toHexString());
                if(target==='#prev_27'){
                    $('.bg_color').css('background-color',color.toHexString());
                }
                if(target==='#prev_39'){
                    $('.prev_tab').css('border-color',color.toHexString());
                }
            }
        }
    });
    
    //Tabs
    var container = document.getElementById("tabContainer");
    var navitem = container.querySelector(".tabs ul li");
    var ident = navitem.id.split("_")[1];
    navitem.parentNode.setAttribute("data-current",ident);
    navitem.classList.add("tabActiveHeader");
    var pages = container.querySelectorAll(".tabpage");
    for (var i = 1; i < pages.length; i++) {
        pages[i].style.display="none";
    }
    var tabs = container.querySelectorAll(".tabs ul li");
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].onclick=displayPage;
    }
    //select preview box that mus be visible for current tab
    $('.tab').on('click',function(){
        var target= this.getAttribute('data-target'); 
        for (var i=0; i<$('.tab').length;i++){
            var data_target = $('.tab')[i].getAttribute('data-target');
            if(data_target===target){
                $($('.tab')[i].getAttribute('data-target')).css('visibility','visible');
                if(data_target!=='#prev_25'){
                    $('#prev_11_image').css('visibility','hidden');
                    if(data_target!=='#prev_27'){
                        $('#prev_12_image').css('visibility','hidden');
                    }else{
                        $('#prev_12_image').css('visibility','visible');
                    } 
                }else{
                    $('#prev_11_image').css('visibility','visible');
                    $('#prev_12_image').css('visibility','hidden');
                }
            }
            else{
                $($('.tab')[i].getAttribute('data-target')).css('visibility','hidden');
                
            }
        } 
    });
    //create the canvas elements to be able to color items
    var canvas = document.getElementById('prev_23');
    var context = canvas.getContext('2d');
    var sources = {
        /*
         * hard coded for now due to cross origin restrictions needs to be 
         * tested with images comming from server
         */
        info: '../images/iconsets/19/themeicon_icon-info (1).png',
        location:'../images/iconsets/19/themeicon_icon-venuetags.png',
        loyalty:'../images/iconsets/19/themeicon_icon-loyalty.png'
    };
    loadImages(sources, function(images) {
        context.drawImage(images.info, -20, 0, 100, 100);
        context.drawImage(images.location, 80, 10, 100, 85);
        context.drawImage(images.loyalty, 190, 0, 100 ,100);
    });
    var headercanvas = document.getElementById('prev_15');
    var headercontext = headercanvas.getContext('2d');
    var sources ={ icon: '../images/navbar_icon.png'};
    loadImages(sources,function(images){
        headercontext.drawImage(images.icon,0,0,38,35);
    });
    /*
     * Remove a image , its preview and reset the input element
     */
    $('.remove_image').on('click',function(){
        //identify the elements that needs to be removed
        var ident = this.previousElementSibling;
        var target= ident.getAttribute('id').split('_small')[0];
        var main_target = document.getElementById(target.split('_image')[0]);
        /*
         * resetting the input element, due to security restrictions it is'nt possible
         * to acces the value of input[file] elements so we remove the element and reappend it
         */
        var parent = main_target.parentNode.innerHTML;
        main_target.parentNode.innerHTML = parent;
        $('#prev_'+ target).attr('src','');
        ident.removeAttribute('src');
        ident.setAttribute('src','');
        /*
         * because the new element does not have an event listener yet we need
         * append the event listener to the element
         */
        $('.image_input').on('change',function(){
            read_preview_URL(this);
        });   
    });
    //show/hide functionality for on/of button (ex splash screen)
    $('.enable_disable').on('click',function(e){
        e.preventDefault();
        $(this).toggleClass('enabled').toggleClass('disabled');
        $('#' + $(this).data('target')).toggle('slow');
        if($(this).hasClass('enabled')){
            $('#preview_header').css('display','none');
        }else{
            $('#preview_header').css('display','block');
        }
    });
});//end $(function(){})
/*
 * Create a preview of an image without uploading it to server first.
 */
function read_preview_URL(input){
    if(input.files && input.files[0]){
        var URL_reader = new FileReader();
        URL_reader.onload = function(e){
            var target = '#prev_' + input.getAttribute('name'); 
            var small_preview = '#' +input.getAttribute('name') + '_small';
            $(target).attr('src',e.target.result).css('visibility','visible');
            $(small_preview).attr('src',e.target.result);
            //remove the tapcrowd logo if new background image is chosen
            if(target==='#prev_11_image'){
                $('#tapcrowd_logo').attr('src','');
            }
        };
        URL_reader.readAsDataURL(input.files[0]);
    }
}
/*
 * add some functionality to the tabs
 */
function displayPage() {
    var current = this.parentNode.getAttribute("data-current");
    //remove class of activetabheader and hide old contents
    document.getElementById("tabHeader_" + current).classList.remove("tabActiveHeader");
    $($('#tabHeader_' + current).attr('data-target') +" img").css('visibility','hidden');
    document.getElementById("tabpage_" + current).style.display="none";
   
    var ident = this.id.split("_")[1];
    //add class of activetabheader to new active tab and show contents
    this.classList.add("tabActiveHeader");
    document.getElementById("tabpage_" + ident).style.display="block";
   
    this.parentNode.setAttribute("data-current",ident);
    
    $(this.getAttribute('data-target') +' img').css('visibility','visible');
    //if preview spash screen is active we need to hide it an reactivate header
    if($('.enable_disable').hasClass('enabled')){
        $('.enable_disable').toggleClass('enabled');
        $('.enable_disable').toggleClass('disabled');
        $('#preview_header').css('display','block');
        $('#prev_4_image').toggle('slow');
    }
}
/*
 * load the images in canvas elements
 */
function loadImages(sources, callback) {
    var images = {};
    var loadedImages = 0;
    var numImages = 0;
    for(var src in sources) {
        numImages++;
    }
    for(var src in sources) {
        images[src] = new Image();
        images[src].onload = function() {
            if(++loadedImages >= numImages) {
                callback(images);
            }
        };
        images[src].src = sources[src];
    }
}