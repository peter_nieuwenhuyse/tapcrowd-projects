-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Genereertijd: 30 jan 2014 om 17:16
-- Serverversie: 5.5.34
-- PHP-versie: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `tapcrowd-test`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `appearance`
--

CREATE TABLE IF NOT EXISTS `appearance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `info` varchar(255) DEFAULT NULL,
  `id_value` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Gegevens worden uitgevoerd voor tabel `appearance`
--

INSERT INTO `appearance` (`id`, `name`, `label`, `info`, `id_value`, `value`, `type`) VALUES
(1, '11_image', 'image', '(recomended width:640px, height832px)', 11, 'http://upload.tapcrowd.com/upload/appimages/3914/launchscreen.png', 110),
(2, '59_image', 'Image for iPhone 5', '(recomended width:640px, height:1136px', 59, 'http://upload.tapcrowd.com/upload/appimages/3914/launchscreen.png', 110),
(3, '25', 'background color', NULL, NULL, 'FFFFFF', 111),
(4, '6_image', 'background image', '', 0, 'http://upload.tapcrowd.com/upload/appimages/3914/navbar.png', 120),
(5, '9_image', 'logo', '(recommended width:400px, height:88px)', 9, '', 120),
(6, '58_image', 'navbar for tablets', '(recommended width: 1548px, height:88px)', 58, NULL, 120),
(7, '61_image', 'Tablet header image', NULL, 61, NULL, 120),
(8, '23', 'launcher screen icon color', NULL, NULL, '000000', 134),
(9, '13', 'Launcher screen icon text color', NULL, NULL, '000000', 132),
(10, '15', 'Navigation bar icon color', NULL, NULL, 'FFFFFF', 134),
(11, '27', 'Phone bg', NULL, NULL, 'FFFFFF', 211),
(12, '46', 'Tablet bg', NULL, NULL, '000000', 211),
(13, '1', 'text color', NULL, NULL, '000000', 212),
(14, '2', 'Separator bg', NULL, NULL, 'f37021', 221),
(15, '8', 'Separator text', NULL, NULL, '000000', 222),
(16, '3', 'Title bg', NULL, NULL, 'f37021', 221),
(17, '7', 'Title text', '', NULL, 'FFFFFF', 222),
(18, '29', 'Subheader bg', NULL, NULL, 'f37021', 221),
(19, '30', 'Subheader Text', NULL, NULL, 'FFFFFF', 222),
(20, '10', 'Selected cell bg', NULL, NULL, '555555', 231),
(21, '33', 'Action cell bg', NULL, NULL, '000000', 231),
(22, '47', 'Premium cell bg', NULL, NULL, 'ffffff', 231),
(23, '48', 'Premium text', NULL, NULL, '000000', 232),
(24, 'separator', '', NULL, NULL, '000000', 233),
(25, '12_image', 'Default image for list', '(recommendedwidth:114px, height114px)', 12, '../images/email-small.png', 240),
(26, '37', 'Background color', '', NULL, '777777', 311),
(27, '38', 'Text color', NULL, NULL, 'FFFFFF', 312),
(28, '39', 'Bg color active', NULL, NULL, '000000', 311),
(29, '40', 'text color active', NULL, NULL, 'FFFFFF', 312),
(30, '16_image', 'Google maps &amp; floorplan marker', 'recommended width:86px, height:104px', 16, '../images/marker-google-maps.png', 320),
(31, '32_image', 'Navigation marker', 'recommended width:86px, height:104px', 32, '../images/marker-floorplan.png', 320),
(32, '49', 'Background color', NULL, NULL, '000000', 331),
(33, '56', 'Text color', NULL, NULL, 'FFFFFF', 332),
(34, '57', 'border color', NULL, NULL, '000000', 333),
(35, '62', 'actionbar background color', NULL, NULL, '000000', 341),
(36, '63', 'actionbar text color', NULL, NULL, '', 342),
(37, '4_image', 'Splashscreen', 'recommended width:640px, height:960px <strong>png image</strong>', 4, '../images/splash4-1.png', 410),
(38, '55_image', 'Splashscreen for iPhone 5:', 'recommended width:640px, height:1136px <strong>png image</strong>', 55, '', 410),
(39, '5_image', 'App icon', 'recommended width:1024px, height:1024px <strong>png image</strong>', 5, '../images/app_icon_ok.png', 410),
(40, '54_image', 'watermark for shared pictures', 'recommended width:612px, height:612px', 54, NULL, 510),
(41, '43', 'Festival timeline view border for artist', NULL, NULL, NULL, 512),
(42, '52', 'header background color for timelines (with hour)', NULL, NULL, NULL, 512),
(43, '53', 'color for line indicating current time', NULL, NULL, NULL, 511),
(44, '50', 'background color for timeline sessions', NULL, NULL, NULL, 511),
(45, '51', 'text color for timeline sessions', NULL, NULL, NULL, 512),
(46, '28', 'background color for date cell', NULL, NULL, NULL, 512),
(47, '31', 'text color for session date cell', NULL, NULL, NULL, 512);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
